/**
 * Created by alexandra on 25.11.2019.
 */
$(document).ready(function () {

    $("[id^='delete']").click(function () {
        var city_value = $(this).attr('id').split('_')[1];
        newFunction(city_value);
    })

    var newFunction = function (location) {
        $.ajax({
            url: '/jsonTest',
            data: {
                'location': location
            },
            dataType: 'json',
            success: function (data) {
                if (data.id) {
                    $('#delete_' + data.id).parent().parent().remove();
                    // alert('A mers')
                }
            }

        });
    };
});