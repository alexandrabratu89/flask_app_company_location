from flask import Blueprint, render_template
from __init__ import db, create_app
from flask_login import login_required, current_user
# from proiect_flask.models import User
import datetime
main = Blueprint('main', __name__)


@main.route('/')
def index():
    return render_template('index.html')


@main.route('/profile')
@login_required
def profile():
    return render_template('profile.html', name=current_user.name, last_login=datetime.datetime.strftime(current_user.last_login, '%d-%m-%Y %H:%M:%S'), phone_number= current_user.phone_number)



if __name__ == '__main__':
    app = create_app()
    app.run(debug=True)