from flask import Blueprint, render_template, redirect, url_for, request, flash
from flask import jsonify
from werkzeug.security import generate_password_hash, check_password_hash
# from proiect_flask.models import User
from __init__ import db
from flask_login import login_user, logout_user, login_required
# from proiect_flask.models import User, Location, Job, Company
from models import User, Company, Location, Job
import datetime
auth = Blueprint('auth', __name__)


@auth.route('/login')
def login():
    return render_template('login.html')


@auth.route('/signup')
def signup():
    return render_template('signup.html')


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('main.index'))


@auth.route('/signup', methods=['POST'])
def signup_post():
    email = request.form.get('email')
    name = request.form.get('name')
    password = request.form.get('password')
    phone_number = request.form.get('phone_number')

    user = User.query.filter_by(email=email).first()
    if user:
        flash('Email already exists')
    elif email is None:
        flash('You need to insert a valid email')
    elif name is None:
        flash("You need to insert a valid name")
    elif password is None:
        flash('Please insert a valid password')
    else:
        new_user = User(email=email, name=name, password=generate_password_hash(password), phone_number=phone_number)

        db.session.add(new_user)
        db.session.commit()

        return redirect(url_for('auth.login'))

    return redirect(url_for('auth.signup'))


@auth.route('/login', methods=['POST'])
def login_post():
    email = request.form.get('email')
    password = request.form.get('password')
    remember = True if request.form.get('remember') else False

    user = User.query.filter_by(email=email).first()

    if not user or not check_password_hash(user.password, password):
        flash('Please check your login details and try again.')
        return redirect(url_for('auth.login'))
    user.last_login = datetime.datetime.now()
    db.session.commit()
    login_user(user, remember=remember)
    return redirect(url_for('main.profile'))


@auth.route('/job')
@login_required
def jobs():
    return render_template('jobs.html')


@auth.route('/company')
@login_required
def company():
    location = Location.query.all()
    return render_template('companies.html', location=location)

@auth.route('/jsonTest')
def ajaxFirst():
    location_id = request.args.get('location')
    try:
        Location.query.filter_by(id=location_id).delete()
        db.session.commit()
        dataSource = {'code': '200', 'id': location_id}
    except Exception:
        dataSource = {'code': '404'}
    return jsonify(dataSource)

@auth.route('/location')
@login_required
def location():
    return render_template('location.html')


@auth.route('/job', methods=['POST'])
@login_required
def jobs_post():
    job_type = request.form.get('type')
    job_url = request.form.get('url')
    job_title = request.form.get('title')
    job_description = request.form.get('description')
    job_how_to_apply = request.form.get('how_to_apply')

    job = Job(type=job_type, url=job_url, title=job_title, description=job_description, how_to_apply=job_how_to_apply)

    db.session.add(job)
    db.session.commit()
    return redirect(url_for('auth.company'))


@auth.route('/company', methods=['POST'])
@login_required
def company_post():
    company_name = request.form.get('name')
    company_web = request.form.get('website')
    company_logo = request.form.get('logo')
    company_location = request.form.get('location')

    company = Company(name=company_name, website=company_web, logo=company_logo, location_id=company_location)

    db.session.add(company)
    db.session.commit()
    return redirect(url_for("auth.location"))


@auth.route('/location', methods=['POST'])
@login_required
def location_post():
    job_location = request.form.get('location')
    all_locations = [x.city for x in Location.query.all()]
    if len(job_location) > 0 and str(job_location).isspace() is False:
        if ''.join(job_location.split(' ')) in all_locations:
            return render_template('location.html', error='Location already exists', location_value=request.form.get('location'))
        else:
            location = Location(city=job_location)

            db.session.add(location)
            db.session.commit()
            # return redirect(url_for('auth.jobs'))
            return redirect(url_for("auth.location_edit"))
            # return render_template('locations_edit.html', city=Location.query.get(location.id))
    elif str(job_location).isspace():
        return render_template('location.html', error='Please try again')

@auth.route('/all_locations', methods=['GET'])
@login_required
def all_locations():
    return render_template('locations_index.html', all_locations=Location.query.all())

@auth.route('/all_companies', methods=['GET'])
@login_required
def all_companies():
    return render_template('companies_index.html', all_companies=Company.query.all())


@auth.route('/location_edit/<int:location_id>', methods=['POST'])
@login_required
def edit_location(location_id):
    if request.method == 'GET':
        job_location = request.form.get('location')
        return render_template('locations_edit.html', city=Location.query.get(location_id))

    elif request.method == 'POST':
        job_location = request.form['location']
        location = Location.query.filter_by(id=location_id).update(dict(city=job_location))
        db.session.commit()
        return render_template('locations_edit.html', city=Location.query.get(location_id))

