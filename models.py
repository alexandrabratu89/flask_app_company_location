from __init__ import db
from flask_login import UserMixin


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    name = db.Column(db.String(1000))
    last_login = db.Column(db.DateTime)
    phone_number = db.Column(db.String(20))


class Location(db.Model):
    id = db.Column(db.Integer, primary_key= True)
    city = db.Column(db.String(100), unique=True)
    companies = db.relationship('Company', backref='location', lazy=True)


class Company(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True)
    website = db.Column(db.String(255))
    logo = db.Column(db.String(255))
    location_id = db.Column(db.Integer, db.ForeignKey('location.id'))
    jobs = db.relationship('Job', backref='company', lazy=True)


class Job(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String(100))
    url = db.Column(db.String(255))
    title = db.Column(db.String(300))
    description = db.Column(db.String(10000))
    how_to_apply = db.Column(db.String(10000))
    company_id = db.Column(db.Integer, db.ForeignKey('company.id'))
